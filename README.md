# ZABBIX - Triggers by Hosts

Python code to extract triggers by hosts from zabbix server

# Before Installation

firt of all you must have python Pandas Modules already installed

if you have pip installed just type pip install pandas, if not:

take a look: https://pandas.pydata.org/pandas-docs/stable/getting_started/install.html

# How it works

Just clone these rep to your machine, not mandatory zabbix server machine, need to be a machine that can reach the zabbix server

Execute the script "get_token.py", with the following syntax:
    ./get_token.py http://your_zabbix_server your_zabbix_user password
It will generate a zabbix token, copy it

Now execute the host_trigger.py, with the following syntax:
    ./host_trigger.py http://your_zabbix_server token_already_pasted
It create a csv file into the folder you're with trigger by hosts information

# Structure of the file generated

First collumn we have host/template name, second collumn there are trigger names

As you can see there are too much template names, cause the zabbix API provide all triggers by templates and hosts, if you need just triggers that are linked to a host you can open this file on excell and filter to get off the templates lines




