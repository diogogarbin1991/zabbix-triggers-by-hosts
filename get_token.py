#!/bin/python

#############################################################
# Autor: Diogo Garbin
# Contact: diogo.garbin@yahoo.com.br
# Description: python script to generate zabbix api access
#############################################################


import json
import sys
import requests

# Parameters

# 1.Zabbix IP or NAME ; 2. Username ; 3.Password


r = requests.post(str(sys.argv[1]) + "/zabbix/api_jsonrpc.php", json={"jsonrpc": "2.0", "method": "user.login", "params": {"user": str(sys.argv[2]) , "password": str(sys.argv[3])}, "id": 1})

rst = json.loads(r.text)
print(rst['result'])

