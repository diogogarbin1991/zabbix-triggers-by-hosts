#!/bin/python

#################################################################
# # Autor: Diogo Garbin                                         #
# # Contato: diogo.garbin@yahoo.com.br                          #
# # Descrição: Extração de relação trigger host                 #
#################################################################

# Argumentos: IP do servidor, Token API


# Modulos

import requests
import json
import sys
import pandas as pd

# Variables

Hostname = [ ]
trname = [ ] 

# Request para baixar informações do zabbix
r = requests.post(str(sys.argv[1])+'/zabbix/api_jsonrpc.php', json={"jsonrpc": "2.0", "method": "trigger.get", "params": {"output":["triggerid", "description"], "selectHosts": ["hostid", "name"]}, "id": 1, "auth": str(sys.argv[2])})

# Gerando lista descrições de triggers
y = json.loads(r.text)
data = y['result']
for w in data:
    trname.append(w['description'])

# Gerando lista de nome de hosts

for x in data:
    nm = x['hosts']
    for k in nm: 
        Hostname.append(k['name'])

# Iniciando\Executando dataframe para criação de tabelas

df = pd.DataFrame({'Host': Hostname,  'Trigger': trname})

df.to_csv('host_and_trigger.csv', index=False, sep='\t', encoding='utf-8')
    

